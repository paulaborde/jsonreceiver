#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Flask, request, render_template, jsonify
import json
import datetime
import os
app = Flask(__name__)
app.config.update(
    HOST='0.0.0.0',
    DEBUG=True)


@app.route('/', methods=["GET", "POST"])
def home():
    if request.method == 'GET':
        actual_path = os.path.split(os.path.realpath(__file__))[0]
        print(actual_path)
        allfiles = os.listdir(actual_path)
        r = [x for x in allfiles if 'json' in x]
        return render_template('home.html', volume=len(r), details=r)
    else:
        data = request.get_json(force=True)
        mytime = datetime.datetime.now()
        filename = 'mappingcible_%s.json' % mytime.strftime("%m%d%Y_%I%M%S")
        with open(filename, 'w') as fp:
            json.dump(data, fp)
        return jsonify({'result':'data stored'})

if __name__ == '__main__':
    app.run()
