# app content
app.py
requirements.txt
Dockerfile

# build container jsonreceiver
docker build -t jsonreceiver:0.2 .
docker login --username=paulaborde --email=paulaborde.g@gmail.com
docker push paulaborde/jsonreceiver

# deploy
docker pull paulaborde/jsonreceiver
docker tag paulaborde/jsonreceiver registry-qt.infra.mdv.openpaas.hpmetier.sf.intra.laposte.fr:80/jsonreceiver:0.2
docker push registry-qt.infra.mdv.openpaas.hpmetier.sf.intra.laposte.fr:80/jsonreceiver:0.2

# container run
docker run --name jsonreceiver -d -p 8000:8000 jsonreceiver:0.2 